from django.db import models

# Create your models here.
class measurement(models.Model):
    node_id = models.IntegerField()
    ts = models.BigIntegerField()
    sen_id = models.IntegerField()
    depth = models.IntegerField()
    