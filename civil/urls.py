from django.urls import path
from civil import views

urlpatterns = [
    path('',views.home,name='home'),
    path('login_auth/',views.login_auth,name='login_auth'),
    path('gateway/',views.login_or_node,name='login'),
    path('waterlevel',views.WaterLevel,name='waterlevel'),
    # path('Node/',views.Node,name='node'),
    path('insertData',views.insertData,name="insertData"),
    path('insertGasData',views.insertGasData,name="insertGasData"),
    path('insertData1',views.insertData1,name="insertData1"),
     path('insertGasData1',views.insertGasData1,name="insertGasData1"),
    path('getData',views.getData,name="getData"),
    path('logout/',views.logout,name='logout'),
]