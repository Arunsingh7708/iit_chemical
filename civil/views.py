from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect
from django.db import connection
from django.urls import reverse
from django.conf import settings
from datetime import datetime as dtime, timedelta, timezone
from waterlevel.models import measurement, GasLevel, ExportCsv
from dateutil import tz
from plotly import offline
from django.http import JsonResponse
# from zoneinfo import ZoneInfo
from backports.zoneinfo import ZoneInfo
from plotly.subplots import make_subplots
from logging.handlers import RotatingFileHandler

import datetime,time, os
import logging
import csv

import chardet,json,timeago,pytz,random
import plotly.graph_objects as go,json

@login_required(login_url='/chemical/login_auth/')
def home(request):
    return render(request, 'index.html')

def login_auth(request):
    return render(request, 'login.html')

@csrf_exempt
def login_or_node(request):
    global IIT__user
    IIT_user = request.POST.get('IIT_username')
    IIT_password = request.POST.get('IIT_password')
    
    request.session.clear_expired()
    if (IIT_user == "admin" and IIT_password == "pwd@admin"):
        request.session['IIT_user'] = IIT_user 
        request.session['IIT_pass'] = IIT_password

        return WaterLevel(request)
    else :
        return render(request, "login.html", {'error_message': 'Invalid username or password'})

def WaterLevel(request):
    session_timeout_seconds = request.session.get_expiry_age()
    if 'IIT_user' not in request.session:    
        return redirect('/chemical/login_auth/')
    else:
        nodes = list(range(2, 8))
        graph_html = None
        selected_node = request.POST.get('node')
        graphs = []
        dt_range = request.POST.get('datetimes')

        if not selected_node:
            selected_node = 2
        # if not dt_range:
        dt_ef = dtime.now(pytz.timezone("Asia/Calcutta")).strftime('%Y-%m-%d %H:%M')
        dt_sf = (dtime.now(pytz.timezone("Asia/Calcutta")) - timedelta(days=1)).strftime('%Y-%m-%d %H:%M')

        s = dt_ef
        e = dt_sf

        dt_e = dtime.strptime(dt_ef, '%Y-%m-%d %H:%M').astimezone(pytz.timezone("UTC"))
        dt_s = dtime.strptime(dt_sf, '%Y-%m-%d %H:%M').astimezone(pytz.timezone("UTC"))

        start_date = round(dt_s.replace(tzinfo=timezone.utc).timestamp())
        end_date = round(dt_e.replace(tzinfo=timezone.utc).timestamp())

        # else:
        #     dates = dt_range.split("-")
        #     # print("DATERANGE:: ",dates)
        #     s = dates[0]
        #     t =  dates[0].strip()
        #     start = t.replace("/","-")
        #     # drs = datetime.strftime(t, '%Y %b %d %H:%M')
        #     last = dtime.strptime(start, '%Y %b %d %H:%M')
        
        #     start_date = last.timestamp()
        #     # start_date -= 19800
            
        #     e = dates[1]
        #     nw = dates[1].strip()
        #     end = nw.replace("/","-")
        #     # dre = datetime.strftime(nw, '%Y %b %d %H:%M')
        #     now = dtime.strptime(end, '%Y %b %d %H:%M')
            
        #     end_date = now.timestamp()
            # end_date -= 19800
        
        # print(round(start_date),round(end_date))

        # timestamps = [dt_s + timedelta(minutes=5*i) for i in range((end_date - start_date) // 300)]

        node_data = measurement.objects.filter(node_id=selected_node,ts__range=(start_date,end_date)).order_by('id')
        sensor_ids = measurement.objects.filter(node_id=selected_node).values_list('sen_id', flat=True).distinct()

    # # seperate graph-------------------------------------------------------------
        # Prepare data for each sensor
        sensor_data = {}
        for sensor_id in sensor_ids:
            sensor_data[sensor_id] = node_data.filter(sen_id=sensor_id)

        # # Generate plots for each sensor
        for sensor_id, data in sensor_data.items():
            print(sensor_data)
            # depths = []
            # for ts in timestamps:
            #     ts = round(ts.timestamp())
            #     # Get the depth for the corresponding timestamp
            #     depth = data.filter(ts=ts).first()
            #     if depth:
            #         depths.append(depth.depth)
            #     else:
            #         depths.append(None)
            timestamps = [dtime.fromtimestamp(data.ts).strftime("%Y-%m-%d %H:%M") for data in data]
            depths = [data.depth for data in data]
            print(depths)
            print(timestamps)
            graph = go.Figure()
            graph.add_trace(go.Scatter(x=timestamps, y=depths, mode='lines+markers', name=f'Sensor {sensor_id}',fill='tozeroy'))
            graph.update_layout(title=f'Depth Data for Sensor {sensor_id}', xaxis=dict(title='Time'), yaxis=dict(title='Depth (cm)'), height=300)
            graph.update_yaxes(range=[0, 300])
            graphs.append(graph.to_html(full_html=False,config={'displaylogo': False}))

        return render(request, 'index.html', {'nodes': nodes, 'graphs': graphs, 'selected_node': selected_node,'dt':f"Date Range Selected : {s} - {e}"})

# -------------------------------------------------------

        # depths = []
        # for i in range(len(list(sensor_ids))):
        #     depths.append([])

        # for sensor_id in sensor_ids:
        # #     sensor_data = node_data.filter(sen_id=sensor_id)
        #     timestamps = [dtime.fromtimestamp(data.ts).strftime("%Y-%m-%d %H:%M") for data in sensor_data]
        #     depths[sensor_id-1].extend([data.depth for data in sensor_data])
        
        # subplot_heights = [0.9] * len(list(sensor_ids))
        # fig = make_subplots(rows=len(list(sensor_ids)), cols=1, shared_xaxes=True, subplot_titles=[f"Sensor {i}" for i in list(sensor_ids)], row_heights=subplot_heights)

        # for i in range(len(list(sensor_ids))):
        #     fig.add_trace(go.Scatter(x=timestamps, y=depths[i], mode='lines+markers', name=f'Sensor {i+1}'), row=i+1, col=1)

        # fig.update_layout(title=f"Depth Data for "+ str(len(list(sensor_ids))) +" Sensors", xaxis=dict(title="Time"), yaxis=dict(title="Depth (cm)"))

        # fig = go.Figure()
        # for sensor_id in sensor_ids:
        #     sensor_data = node_data.filter(sen_id=sensor_id)
        #     timestamps = [dtime.fromtimestamp(data.ts).strftime("%Y-%m-%d %H:%M") for data in sensor_data]
        #     depths = [data.depth for data in sensor_data]
        #     # print(timestamps)
        #     # print(depths)
        #     fig.add_trace(go.Scatter(x=timestamps, y=depths, mode='lines+markers', name=f'Sensor {sensor_id}'))
        
        # fig.update_layout(title=f'Depth Data for Node {selected_node}', xaxis=dict(title='DateTime'), yaxis=dict(title='Depth(cm)'))
        # # fig.update_yaxes(range=[0, 1000])

        # graph_html = fig.to_html(full_html=False, config={'displaylogo': False})

        # return render(request, 'index.html', {'nodes': nodes, 'graph_html': graph_html, 'selected_node': selected_node, 'dt':f"Date Range Selected : {s} - {e}"})

def Graph(request):
    selected_node = request.POST.get('node')
    graphs = []
    dt_range = request.POST.get('datetimes')

    dates = dt_range.split("-")
    
    s = dates[0]
    t =  dates[0].strip()
    start = t.replace("/","-")
    last = dtime.strptime(start, '%Y %b %d %H:%M')

    start_date = last.timestamp()
    # start_date -= 19800
    
    e = dates[1]
    nw = dates[1].strip()
    end = nw.replace("/","-")
    now = dtime.strptime(end, '%Y %b %d %H:%M')
    
    end_date = now.timestamp()
    # end_date -= 19800

    node_data = measurement.objects.filter(node_id=selected_node,ts__range=(start_date,end_date)).order_by('id')
    sensor_ids = measurement.objects.filter(node_id=selected_node).values_list('sen_id', flat=True).distinct()

    sensor_data = {}
    for sensor_id in sensor_ids:
        sensor_data[sensor_id] = node_data.filter(sen_id=sensor_id)

    # # Generate plots for each sensor
    for sensor_id, data in sensor_data.items():
        timestamps = [dtime.fromtimestamp(data.ts).strftime("%Y-%m-%d %H:%M") for data in data]
        depths = [data.depth for data in data]
        print(depths)
        print(timestamps)
        graph = go.Figure()
        graph.add_trace(go.Scatter(x=timestamps, y=depths, mode='lines+markers', name=f'Sensor {sensor_id}',fill='tozeroy'))
        graph.update_layout(title=f'Depth Data for Sensor {sensor_id}', xaxis=dict(title='Time'), yaxis=dict(title='Depth (cm)'), height=300)
        graph.update_yaxes(range=[0, 300])
        graphs.append(graph.to_html(full_html=False,config={'displaylogo': False}))

    return JsonResponse({'fig':graphs})

def setup_logger(request, logger_name, log_file, level=logging.DEBUG):
    log_file = settings.MEDIA_ROOT+"/"+log_file
    tz = pytz.timezone("Asia/Calcutta")
    now = dtime.now(tz)
    log_setup = logging.getLogger(logger_name)
    while log_setup.hasHandlers():
        log_setup.removeHandler(log_setup.handlers[0])

    formatter = logging.Formatter(str(now)+'- [%(name)s] - Path :-['+request.get_full_path()+'] - [%(levelname)s] - %(message)s', datefmt='%d-%m-%Y %H:%M:%S %p %Z')
    logging.Formatter.converter = time.localtime
    fileHandler = logging.handlers.RotatingFileHandler(log_file, mode='a', maxBytes=1*1024*1024, backupCount=1)
    os.chmod(log_file , 0o777)
    fileHandler.setFormatter(formatter)
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    log_setup.setLevel(level)
    log_setup.addHandler(fileHandler)
    log_setup.addHandler(streamHandler)
#-----------------------------------------------------------------------------------------

def logger(msg, level, logfile):

    if logfile == '1'   : log = logging.getLogger('1')
    if logfile == '0'   : log = logging.getLogger('0') 
    if level == 'info'    : log.info(msg) 
    if level == 'warning' : log.warning(msg)
    if level == 'error'   : log.error(msg)
    if level == 'debug'   : log.error(msg)   

def Export(request):
    nodes = list(range(2, 8))
    f_detail = ExportCsv.objects.order_by('-id')[:10]

    if request.method == 'POST':

        dt_range = request.POST.get('datetimes')
        selected_node = request.POST.get('node')

        dates = dt_range.split("-")
        
        s = dates[0]
        t =  dates[0].strip()
        start = t.replace("/","-")
        last = dtime.strptime(start, '%Y %b %d %H:%M')

        start_date = last.timestamp()
        # start_date -= 19800
        
        e = dates[1]
        nw = dates[1].strip()
        end = nw.replace("/","-")
        now = dtime.strptime(end, '%Y %b %d %H:%M')
        
        end_date = now.timestamp()
        # end_date -= 19800

        data = measurement.objects.filter(ts__range=[start_date, end_date],node_id=selected_node)

        current_datetime = dtime.now()

        updated_datetime = current_datetime + timedelta(hours=5, minutes=30)

        formatted_datetime = updated_datetime.strftime('%d-%m-%Y %H:%M:%S')
        
        s_t = dtime.strptime(start, '%Y %b %d %H:%M')
        s_t = s_t.strftime('%d%m%Y%H%M')
        e_t = dtime.strptime(end, '%Y %b %d %H:%M')
        e_t = e_t.strftime('%d%m%Y%H%M')
        filename = f"node{selected_node}_{s_t}-{e_t}.csv"

        response = HttpResponse(content_type='text/csv; charset=utf-8')
        response['Content-Disposition'] = f'attachment; filename="{filename}"'

        writer = csv.writer(response)
        writer.writerow(['node_id', 'sen_id', 'depth','datetime'])

        for row in data:
            # print(row.node_id, row.sen_id, row.depth, dtime.utcfromtimestamp(row.ts).strftime('%d-%m-%Y %H:%M:%S'))
            writer.writerow([row.node_id, row.sen_id, row.depth, dtime.fromtimestamp(row.ts).strftime('%d-%m-%Y %H:%M:%S')])

        expt = ExportCsv.objects.create(file_name=filename,date_time=formatted_datetime)
        expt.save()
        
        return response

    return render(request, 'export.html',{'nodes': nodes,'f_detail':f_detail})

@csrf_exempt
def insertData(request):
    content_length = request.META.get('CONTENT_LENGTH')
    if content_length:
        try:
            print("length of http request",content_length)
        except ValueError:
            pass
    stf = 0
    encoding = chardet.detect(request.body)
    if encoding['encoding'] == "ascii":
        json_body = json.loads(request.body.decode("utf-8"), strict=False)       
    else:
        json_body = request.body.decode("iso-8859-1")

    path = "logs/IIT_chemical.log"
    setup_logger(request, str(stf), path)
    logger(''+" ("+str(json_body)+")", 'info', str(stf))

    # print(json_body)
    if "HANDSHAKE" in str(json_body):
        return JsonResponse("handshake available",safe=False)

    elif "Capabilities" in str(json_body):
        return JsonResponse("Capabilities available",safe=False)
    
    elif "SYSCONFIG" in str(json_body):
        return JsonResponse("sysconfig available",safe=False)

    elif "SensorDataRecord" in str(json_body):       
        
        da = json_body.get('SensorDataRecord').get('DataRecord')
        # print(json_body)
        for i in da:
            if i.get('SID') == '9':
                ts = i.get('TS')
                hc12 = i.get('HC12RESP').split('.')
                for i in hc12:
                    node_id = i.split('@')[0].split('#')[0].split('$')[-1]
                    sen_id = i.split('@')[0].split('#')[-1]
                    depth = i.split('@')[-1]
                    print(node_id,sen_id,depth)
                    if node_id and sen_id and depth:
                        node_id = int(node_id)
                        sen_id = int(sen_id)
                        depth = int(depth)

                        dt = measurement.objects.create(node_id=node_id,ts=ts,sen_id=sen_id,depth=depth)
                        dt.save()
  
        return JsonResponse("Sensordatarecord available",safe=False)

    else:
        return JsonResponse("empty",safe=False)

    return HttpResponse("success")

@csrf_exempt
def insertGasData(request):
    encoding = chardet.detect(request.body)
    if encoding['encoding'] == "ascii":
        json_body = json.loads(request.body.decode("utf-8"), strict=False)       
    else:
        json_body = request.body.decode("iso-8859-1")

    print(json_body)
    if "HANDSHAKE" in str(json_body):
        return JsonResponse("handshake available",safe=False)

    elif "Capabilities" in str(json_body):
        return JsonResponse("Capabilities available",safe=False)
    
    elif "SYSCONFIG" in str(json_body):
        return JsonResponse("sysconfig available",safe=False)

    elif "SensorDataRecord" in str(json_body):
        da = json_body.get('SensorDataRecord').get('DataRecord')
        print(json_body)
        for i in da:
            if i.get('SID') == '9':
                ts = i.get('TS')
                hc12 = i.get('HC12RESP').split('.')
                highest_gas_level = 0 
                for entry in hc12:
                    node_id, sen_id, gas_lvl = entry.split('@')[0].split('#')[0].split('$')[-1], entry.split('@')[0].split('#')[-1], entry.split('@')[-1]
                    
                    if node_id and sen_id and gas_lvl:
                        try:
                            GasLvl = int(gas_lvl)
                        except ValueError:                            
                            continue
                        if GasLvl > highest_gas_level:
                            highest_gas_level = GasLvl
                if highest_gas_level >= 500:
                    alert = 1
                else:
                    alert = 0
               
                dt = GasLevel.objects.create(ts=ts, gaslevel=highest_gas_level, alert=alert)
                dt.save()

  
        return JsonResponse("Sensordatarecord available",safe=False)

    else:
        return JsonResponse("empty",safe=False)

    return HttpResponse("success")

flg = True

def getData(request):  
    global flg
    encoding = chardet.detect(request.body)
    if encoding['encoding'] == "ascii":
        json_body = json.loads(request.body.decode("utf-8"), strict=False)       
    else:
        json_body = request.body.decode("iso-8859-1")
    stf = 1
    
    if flg:
        mess = {
        "CODE": "GFR",
        "ID": "2400G001402J4FMS",
        "HATCFG": {
            "SENSORCFG": {
                "BQ25611D": {
                    "status": "DISABLED"
                },
                "LTC2942": {
                    "status": "DISABLED"
                },
                "GPSL86": {
                    "status": "DISABLED"
                }
            }
        }
        }

        flg = False

    else:
        mess = {'CODE': 'GFR', 'ID': '2400G001402J4FMS', 'CMD': 'NONE'}
    path = "logs/IIT_chemical.log"
    setup_logger(request, str(stf), path)
    logger(''+" ("+str(mess)+")", 'info', str(stf))
    return JsonResponse(mess,safe=False)

def logout(request):
    return HttpResponseRedirect('/chemical/')

# simulator------------------------------------------------

@csrf_exempt
def insertData1(request):
    encoding = chardet.detect(request.body)
    if encoding['encoding'] == "ascii":
        json_body = json.loads(request.body.decode("utf-8"))       
    else:
        json_body = request.body.decode("iso-8859-1")

    print(json_body)
    if json_body.get('SID') == '9':
        ts = json_body.get('TS')
        hc12 = json_body.get('HC12RESP').split('.')
        for i in hc12:
            node_id = i.split('@')[0].split('#')[0].split('$')[-1]
            sen_id = i.split('@')[0].split('#')[-1]
            depth = i.split('@')[-1]
            if node_id and sen_id and depth:
                node_id = int(node_id)
                sen_id = int(sen_id)
                depth = int(depth)

                dt = measurement.objects.create(node_id=node_id,ts=ts,sen_id=sen_id,depth=depth)
                dt.save()
    return HttpResponse(json_body)

@csrf_exempt
def insertGasData1(request):
    encoding = chardet.detect(request.body)
    if encoding['encoding'] == "ascii":
        json_body = json.loads(request.body.decode("utf-8"))       
    else:
        json_body = request.body.decode("iso-8859-1")

    print(json_body)
    if json_body.get('SID') == '9':
        ts = json_body.get('TS')
        hc12 = json_body.get('HC12RESP').split('.')
        for i in hc12:
            node_id = i.split('@')[0].split('#')[0].split('$')[-1]
            sen_id = i.split('@')[0].split('#')[-1]
            gas_lvl = i.split('@')[-1]
            if node_id and sen_id and gas_lvl:
                GasLvl = int(gas_lvl)
                if GasLvl >= 500:
                    alert = 1
                else:
                    alert = 0
                dt = GasLevel.objects.create(ts=ts,gaslevel=GasLvl,alert=alert)
                dt.save()
    return HttpResponse(json_body)

def ViewLog(request):
    filename = request.GET.get('log')    
    log_file = settings.MEDIA_ROOT+"/"
    response = HttpResponse(open("logs/"+filename, 'rb').read())
    response['Content-Type'] = 'text/plain'
   
    return response

# for node_id in range(2, 8):
#             for _ in range(6):  # Only 6 sensor IDs per node
#                 for i in range(72):  # 3 days (72 hours)
#                     ts = datetime.datetime.now() - datetime.timedelta(days=3) + datetime.timedelta(hours=i)
#                     sensor_id = random.randint(1, 99)
#                     depth = random.randint(100, 999)
#                     SensorData.objects.create(node_id=node_id, timestamp=ts, sensor_id=sensor_id, depth=depth)


