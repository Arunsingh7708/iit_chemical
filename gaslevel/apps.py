from django.apps import AppConfig


class GaslevelConfig(AppConfig):
    name = 'gaslevel'
