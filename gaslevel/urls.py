from django.urls import path
from gaslevel import views

urlpatterns = [
    path('',views.home_gas,name='home1'),
    path('login_auth/',views.login_auth_gas,name='login_auth1'),
    path('Data/',views.login_or_node_gas,name='login1'),
    path('logout/',views.logout_gas,name='logout1'),
]