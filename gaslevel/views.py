from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from datetime import datetime as dtime, timedelta, timezone
from waterlevel.models import GasLevel
from backports.zoneinfo import ZoneInfo

import plotly.offline as opy
import plotly.graph_objects as go
import pytz,timeago

#gaslvel----------------------------------------------------------

@login_required(login_url='/gaslevel/login_auth/')
def home_gas(request):
    return render(request, 'index.html')

def login_auth_gas(request):
    return render(request, 'login_gas.html')

@csrf_exempt
def login_or_node_gas(request):
    gas_username = request.POST.get('GAS_username')
    gas_password = request.POST.get('GAS_password')

    if (gas_username == "admin" and gas_password == "pwd@admin") or ('gaslevel_user' in request.session):
        request.session['gaslevel_user'] = gas_username 
        request.session['gaslevel_pass'] = gas_password

        dt_range = request.POST.get('datetimes')
         
        if not dt_range:
            dt_ef = dtime.now(pytz.timezone("Asia/Calcutta")).strftime('%Y-%m-%d %H:%M')
            dt_sf = (dtime.now(pytz.timezone("Asia/Calcutta")) - timedelta(days=1)).strftime('%Y-%m-%d %H:%M')

            s = dt_ef
            e = dt_sf

            dt_e = dtime.strptime(dt_ef, '%Y-%m-%d %H:%M').astimezone(pytz.timezone("UTC"))
            dt_s = dtime.strptime(dt_sf, '%Y-%m-%d %H:%M').astimezone(pytz.timezone("UTC"))

            start_date = round(dt_s.replace(tzinfo=timezone.utc).timestamp())
            end_date = round(dt_e.replace(tzinfo=timezone.utc).timestamp())

        else:
            dates = dt_range.split("-")
            print("DATERANGE:: ",dates)
            s = dates[0]
            t =  dates[0].strip()
            start = t.replace("/","-")
            # drs = datetime.strftime(t, '%Y %b %d %H:%M')
            last = dtime.strptime(start, '%Y %b %d %H:%M')
        
            start_date = last.timestamp()
            # start_date -= 19800
            
            e = dates[1]
            nw = dates[1].strip()
            end = nw.replace("/","-")
            # dre = datetime.strftime(nw, '%Y %b %d %H:%M')
            now = dtime.strptime(end, '%Y %b %d %H:%M')
            
            end_date = now.timestamp()
            # end_date -= 19800
        
        print(round(start_date),round(end_date))
        gas_data = GasLevel.objects.latest('id')
        print(gas_data)
        latest_data = gas_data.alert
        gas_l = gas_data.gaslevel
        cur_time = dtime.now(tz=ZoneInfo('Asia/Kolkata')).strftime('%Y-%m-%d %H:%M:%S')
        gas_ts = dtime.fromtimestamp(gas_data.ts).strftime("%Y-%m-%d %H:%M:%S")
        time_ago = timeago.format(gas_ts,cur_time)

        gas_data1 = GasLevel.objects.filter(ts__range=(start_date,end_date)).order_by('id')
    
        g_alert = []
        timestamps = []
        for i in gas_data1:
            timestamps.append(dtime.fromtimestamp(i.ts).strftime("%Y-%m-%d %H:%M"))
            g_alert.append(i.alert)

        trace = {
                    "x": timestamps,
                    "y": g_alert,
                    "line": {"shape": 'hv'},
                    "mode": 'lines',
                    "name": 'AlertStatus',
                    "type": 'scatter',
                    "line_color":'#FF0000',
                    "line_width": 1
                    }
        
        data = [trace]
        layout = go.Layout(height= 120, margin=go.layout.Margin(l=0, r=0, b=0, t=0), font=dict(size=9))
        figure = go.Figure(data = data, layout = layout)
        figure.update_yaxes(fixedrange=False,automargin=False,rangemode="tozero")
        #gsmfigure.update_xaxes(tickangle=-90)
        figure.update_xaxes(nticks=10)
        figure.update_layout(
            yaxis = dict(
                tickmode = 'linear',
                tick0 = 1,
                dtick = 1
            )
        )    
    
        plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'displayModeBar': False})

        return render(request, "index_gas.html", {'dt':f"Date Range Selected : {s} - {e}", 'plot_html':plot_div, 'latest':latest_data, 'gas_l':gas_l, 'gas_ts':time_ago})

    else:
        return render(request, "login_gas.html", {'error_message': 'Invalid username or password'})
    
def logout_gas(request):
    return HttpResponseRedirect('/gaslevel/')