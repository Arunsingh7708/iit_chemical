from django.apps import AppConfig


class WaterlevelConfig(AppConfig):
    name = 'waterlevel'
