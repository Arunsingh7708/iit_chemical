from django.db import models

# Create your models here.
class measurement(models.Model):
    node_id = models.IntegerField()
    ts = models.BigIntegerField()
    sen_id = models.IntegerField()
    depth = models.IntegerField()

class GasLevel(models.Model):
    ts = models.BigIntegerField()
    gaslevel =models.IntegerField()
    alert = models.IntegerField()

class ExportCsv(models.Model):
    file_name = models.CharField(max_length=200)
    date_time = models.CharField(max_length=200)
